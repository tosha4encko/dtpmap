import { Map, Feature, View, MapBrowserEvent, Overlay } from 'ol';
import { defaults as defaultControls } from 'ol/control';
import { GeoJSON } from 'ol/format';
import { Point, Geometry } from 'ol/geom';
import { bbox } from 'ol/loadingstrategy';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import OverlayPositioning from 'ol/OverlayPositioning';
import { toLonLat } from 'ol/proj';
import { OSM, Vector as VectorSource } from 'ol/source';
import { Circle, Fill, Style } from 'ol/style';
import IFeature from '../interfaces/IFeature';

export default class DTPMap{
    private map: Map;
    public features: IFeature[] = [];
    private popup = new Overlay({
        offset: [0, -5],
        positioning: OverlayPositioning.BOTTOM_CENTER,
    });
    private vectorSource: VectorSource;
    private filters: string = "";
    private controller = new AbortController();
    private showPopup = (evt:MapBrowserEvent<UIEvent>) => {};
    private showCard = (evt:MapBrowserEvent<UIEvent>) => {};
    private changeIsFetching = (bool:boolean) => {};
    private changeСurrentFeatures = (features:IFeature[]) => {};
    private throwError = (error: string) => {};

    public constructor(center = [39.69, 47.23], zoom = 15) {
        const refreshСurrentInfo = (extent: [number, number, number, number]) => {
            const features = this.vectorSource.getFeaturesInExtent([extent[0], extent[1], extent[2], extent[3]]);
            const featuresInfo: IFeature[] = [];

            features.forEach((elem) => {
                featuresInfo.push(this.features.find( (elem2) => {
                    if (elem2.id === elem.getId()) {

                        return elem2;
                    };
                })! as IFeature)      
            });
            this.changeСurrentFeatures(featuresInfo);
        }

        function chooseColor(severity:string) {
            switch (severity) {
                case ("Легкий"):
                    return "#008080";
                case ("Тяжелый"):
                case ("Тяжёлый"):
                    return "#e92828";
                case ("С погибшими"):
                    return "#6c0000";
                default:
                    return "";
                };
        };
    
        function addStyleBySeverity(feature: Feature<Geometry>, severity:string){
            const fill = new Fill();
            const color = chooseColor(severity);
    
            if (color === "") {
                throw new Error("Incorrect severty");
            }
            fill.setColor(color);
            feature.setStyle(
                new Style({
                    image: new Circle({
                        radius: 6,
                        fill,
                    }),
                })
            );
        };

        this.vectorSource = new VectorSource({
            format: new GeoJSON(),
                strategy: bbox,
            });
        this.map = new Map({
            layers: [
                new TileLayer({
                    source: new OSM(),
                }),
                new VectorLayer({
                    source: this.vectorSource,
                }),
            ],
            view: new View({
                center,
                zoom,
            }),
            controls : defaultControls({
                zoom : false,
            }),
        });

        this.vectorSource.setLoader((extent) => {
            if(this.filters !== ""){
                const url = `${process.env.REACT_APP_API_URL}api/v1/dtp/?${this.filters}&bbox=${extent[0]},${extent[1]},${extent[2]},${extent[3]}`;

                this.changeIsFetching(true);
                fetch(url, { signal: this.controller.signal }).then(async response => {
                    const data = await response.json();
                        
                    if (response.ok) {
                        let mapFeatures: Feature<Geometry>[] = [];

                        for (const elem of data.features) {
                            if (elem.properties.severity !== "Без пострадавших") {
                                if (this.vectorSource.getFeatureById(elem.id) === null) {
                                    const feature = new Feature({
                                        geometry: new Point(elem.geometry.coordinates),
                                    });
                                    feature.setId(elem.id);
    
                                    try {
                                        addStyleBySeverity(feature,elem.properties.severity);
                                    } catch(e){
                                        this.throwError(`Incorrect severty: ${elem.id} ${elem.properties.severity}`);
                                        return;
                                    };
                                    this.features.push(elem);
                                    mapFeatures.push(feature);
                                };
                            };
                        };
                        this.vectorSource.addFeatures(mapFeatures);
                        this.changeIsFetching(false);
                        const firstCoordinate = this.map.getCoordinateFromPixel([0,0])
                        const secondCoordinate = this.map.getCoordinateFromPixel([document.documentElement.clientWidth, document.documentElement.clientHeight])
                        refreshСurrentInfo([firstCoordinate[0], secondCoordinate[1], secondCoordinate[0], firstCoordinate[1]]);
                    } else {
                        this.throwError("Response is not ok");
                    };
                }).catch((e) => {
                    if (`${e.name}` !== "AbortError"){
                        this.throwError("Incorrect adres");
                    }
                });
            };
        });
        this.map.addOverlay(this.popup);
        this.map.on("moveend", (evt) => {
            const firstCoordinate = toLonLat([evt.frameState.extent[0], evt.frameState.extent[1]]);
            const secondCoordinate = toLonLat([evt.frameState.extent[2], evt.frameState.extent[3]]);

            refreshСurrentInfo([firstCoordinate[0], firstCoordinate[1], secondCoordinate[0], secondCoordinate[1]])
        });
    };

    public setTarget(targrt:string) {
        this.map.setTarget(targrt);
    };

    public setFilters(filters: string){
        this.controller.abort();
        this.controller = new AbortController();
        this.filters = filters;
        this.vectorSource.clear();
        this.features = [];
        this.vectorSource.refresh();
    }

    public setPopupElement(element: HTMLElement){
        this.popup.setElement(element);
    };

    public setPopupPosition(position: number[]){
        this.popup.setPosition(position);
    };

    public getFeatures(){
        return [...this.features];
    };

    public findFeature(pixel: number[]){
        return this.map.forEachFeatureAtPixel(pixel, function (feature) {
            return feature! as Feature;
        });
    };

    public setShowPopup(listener: (evt: MapBrowserEvent<UIEvent>) => void) {
        this.showPopup = listener;
    };

    public setShowCard(listener: (evt: MapBrowserEvent<UIEvent>) => void) {
        this.showCard = listener;
    }

    public setChangeIsFetching(listener: (bool: boolean) => void){
        this.changeIsFetching = listener;
    }

    public setChangeСurrentFeatures(listener: (features: IFeature[]) => void){
        this.changeСurrentFeatures = listener;
    }
    public setThrowError(listner:(error: string) => void){
        this.throwError = listner;
    }

    public unlockPopup(){
        this.map.on('pointermove', this.showPopup);
    }

    public lockPopup(){
        this.map.un('pointermove', this.showPopup);
    }
    
    public unlockCard(){
        this.map.on('click', this.showCard);
    }

    public lockCard(){
        this.map.un('click', this.showCard);
    }

}