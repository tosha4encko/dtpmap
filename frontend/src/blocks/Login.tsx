import React, { useState } from "react";
import ILogin from '../interfaces/ILogin';
import Cookies from 'js-cookie';
import { Form, Input, Button, Card, notification, Modal, Typography } from 'antd';
const { Title } = Typography;

export default function Login(props: ILogin){
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    function authentication() {
        if (username !== "" && password !== "") {
            const params = {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                  },
                body: JSON.stringify({
                    username,
                    password,
                }),
            };

            fetch(`${process.env.REACT_APP_API_URL}api/token/`, params).then(async response => {
                const data = await response.json();
                if (response.ok) {
                    Cookies.set("accessToken", data.access);
                    Cookies.set("refreshToken", data.refresh, {expires: 1});
                    props.setAccessToken(data.access);
                    props.setRefreshToken(data.refresh);
                    props.setAllowEnter(true);
                } else {
                    notification.warning({
                        message: "Не верный логин или пароль",
                        description: "Проверьте CapsLock",
                    });
                };
            }).catch( () => {
                Modal.error({
                    title: 'У нас возникла ошибка',
                    content: `Подождите, cкоро починим`,
                    okButtonProps: { style:{ display:"none" }},
                    keyboard:false,
                });
            });
        } else {
            notification.warning({
                message:"Логин или пароль пуст",
            });
        };
    };

    return(
        <Card style={{
            display: "grid",
            placeItems: "center",
        }}>
            <Form layout="vertical">
                <Title>Вход</Title>
                <Form.Item label="Имя пользователя">
                    <Input
                        type="text"
                        onChange={(event) => {
                            setUsername(event.target.value);
                        }}
                    />
                </Form.Item>
                <Form.Item label="Пароль">
                    <Input
                        type="password"
                        onChange={(event) => {
                            setPassword(event.target.value);
                        }}
                    />
                </Form.Item>
                <Form.Item>
                    <Button 
                        type="primary"
                        htmlType="submit" 
                        onClick={() => {
                            authentication();
                        }}
                    >Войти</Button>
                </Form.Item>
            </Form>
        </Card>
    );
};