import React from 'react';
import IProperties from '../../interfaces/IProperties';
import {Typography, Space } from 'antd';
const {Text} = Typography;

export default function Tegs(props: IProperties) {

    return(
            <Space 
                direction="vertical"
                style={{
                    marginTop: "33px",
                    width: "fit-content"
                }}
            >
                <Text className="title">Транспорт: {props.transports.length}</Text>
                <Text className="title">Участники: {props.count_participants}</Text>
                {props.count_death > 0 ?
                    <Text
                        className="title"
                        style={{
                            color: "#6c0000"
                        }}
                    >Погибло: {props.count_death}</Text>
                : <></>}
                {props.count_injured > 0 ?
                    <Text
                        className="title" 
                        style={{
                            color: props.severity === "Легкий" ? "#008080" : "#e92828"
                        }}
                    >Пострадало: {props.count_injured}</Text>
                : <></>}
                <Text className="title">{props.light}</Text>
                {props.weather.length > 0 ?
                    <>
                        <Text className="title">Погода: {props.weather.length === 1 ? props.weather[0].toLowerCase() : ""}</Text>
                        {props.weather.length > 1 ? 
                            <Space direction="vertical">
                                {props.weather.map((elem, index) => 
                                    <Text className="title" key={index}>• {elem}</Text>
                                )}
                            </Space>
                        : <></> }
                    </>
                : <></>}
            </Space>
    )
}