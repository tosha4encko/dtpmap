import React from 'react';
import { Typography, Space } from 'antd';
import moment from 'moment';
import IOverlayInfo from '../../interfaces/IOverlayInfo';
const { Text } = Typography;

export default function Heders(props: IOverlayInfo) {

    return(
        <Space direction="vertical">
            <Text className="title" strong>
                {props.properties.category}
            </Text>
            <Text className="title">{moment(props.properties.datetime).format("DD.MM.YYYY hh:mm")} | {props.properties.address}</Text>
        </Space>
    )
}