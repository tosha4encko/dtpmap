import React from "react";
import IProperties from '../../interfaces/IProperties';
import Transport from "./Transport";
import Pedestrians from "./Pedestrians";
import { Space, Typography } from 'antd';
const { Text } = Typography;

export default function MembersContainer(props:IProperties) {
    const guiltyTransoprts = props.transports.filter((elem,index) => {
        if (elem.participants.find((elem) => {
            if (elem.violations.length > 0){

                return elem
            };
        }) !== undefined){

            return elem;
        };
    });
    const guiltyPedestrians = props.participants.filter((elem) => {
        if (elem.violations.length > 0){

            return elem;
        };
    });
    const injuredTransoprts = props.transports.filter((elem,index) => {
        if (elem.participants.find((elem) => {
            if (elem.violations.length > 0){

                return elem;
            };
        }) === undefined){

            return elem;
        };
    });
    const injuredPedestrians = props.participants.filter((elem) => {
        if (elem.violations.length === 0){

            return elem;
        };
    });

    return(
        <Space 
            align="start" 
            size={[100,0]}
            style={{
                display: "flex", 
                justifyContent: "space-around",
                padding: "0px 10px 10px 150px"
            }}
        >
            {guiltyTransoprts.length + guiltyPedestrians.length > 0 ?
                <Space 
                    direction="vertical"
                    style={{
                        marginLeft: "100px",
                    }}
                >
                    <Text className="title">Виновник{guiltyTransoprts.length + guiltyPedestrians.length > 1 ? "и" : ""} ДТП</Text>
                    <Space 
                        direction="horizontal" 
                        align="start"
                        size={[20,0]}
                    >
                        {guiltyTransoprts.map((elem, index) => {
            
                            return(
                                <Transport key={index} {...elem}/>
                            )    
                        })}
                        {guiltyPedestrians.length > 0 ?
                            <Pedestrians participants={guiltyPedestrians} /> 
                        : <></>}
                    </Space>
                </Space>
            : <></>}
            {injuredTransoprts.length + injuredPedestrians.length > 0 ?
                <Space direction="vertical">
                    <Text className="title">Участник{injuredTransoprts.length + injuredPedestrians.length > 1 ? "и" : ""} ДТП</Text>
                    <Space 
                        direction="horizontal" 
                        align="start"
                        size={[20,0]}
                    >
                        {injuredTransoprts.map((elem, index) => {
            
                            return(
                                <Space key={index}>
                                    <Transport {...elem}/>
                                </Space>
                            )    
                        })}
                        {injuredPedestrians.length > 0 ?
                            <Pedestrians participants={injuredPedestrians} /> 
                        : <></>}
                    </Space>
                </Space>
            : <></>}
            
        </Space>
    );
};