import React from "react";
import ITransport from '../../interfaces/ITransport';
import Participant from "./Participant";
import {Card, Typography, List, Space, Divider } from 'antd';
const { Text } = Typography;

export default function Transport(props:ITransport) {
    const driver = props.participants.find((elem) => {
        if (elem.role === "Водитель"){
            
            return elem;
        };
    });

    return(
        <Card
            className="card"             
        >
            { driver !== undefined ? 
                <Space direction="vertical">
                    <Text>
                        {props.brand}, {props.year}, {props.color}
                    </Text>
                    <Participant {...driver}/>
                </Space>
            : <></> }
            <List
                grid={{column:1}}
                dataSource={props.participants}
                renderItem={(item) =>{
                    if(item.role !== "Водитель"){

                        return(
                            <Space direction="vertical">
                                <Divider/>
                                <Participant {...item}/>
                            </Space>
                        )
                    }
                }}
            />
        </Card>
    );
};