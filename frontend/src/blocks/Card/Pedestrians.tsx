import React from "react";
import IParticipants from '../../interfaces/IParticipants';
import Participant from "./Participant";
import { Card, List, Space } from 'antd';

export default function Pedestrian(props: IParticipants) {

    return(
        <Card
            className="card"
            style={{
                width: "225px",
            }}
        >
            <List
                grid={{column:1}}
                dataSource={props.participants}
                renderItem={(item) =>
                    <Space direction="vertical">
                        <Participant {...item}/>
                    </Space>
                }
            />
        </Card>
    );
};