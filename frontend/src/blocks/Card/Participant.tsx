import React from "react";
import IParticipant from '../../interfaces/IParticipant';
import { InfoCircleOutlined } from '@ant-design/icons';
import { Typography, Space, Image } from 'antd';
const { Text, Paragraph } = Typography;

export default function Participant(props:IParticipant){
    const statusWord = props.health_status.split(" ")[0];

    return(
        <Space direction="vertical">
            <Space direction="horizontal">
                <Image 
                    width={40}
                    preview={false}
                    src={`./img/${props.role === "Водитель" ? "Driver" : "People"}.png`}
                />
                <Space direction="vertical">
                    <Text>
                        {props.role}
                    </Text>
                    <Text>
                        {props.gender}
                    </Text>
                </Space>
            </Space>

            {props.violations.length > 0 ? 
                <Space 
                    direction="vertical"
                    style={{
                        paddingLeft: "5px",
                        backgroundColor:"lightblue",
                    }}
                >
                    {props.violations.map((elem, index) => 
                        <Space key={index} >
                            <InfoCircleOutlined />
                                <Text>
                                    {elem}
                                </Text>
                        </Space>
                    )}                    
                </Space>
            : <></>}
            <Paragraph 
                style={{
                    paddingLeft: "5px",
                    cursor: "default",
                    backgroundColor: statusWord === "Скончался" ? "rgb(255, 0, 0, 0.8)" : statusWord === "Раненый," ? "rgb(255, 69, 0, 0.8)" : statusWord === "Получил" ? "rgb(0, 128, 128,  0.8)" : "rgb(128, 128, 128, 0.8)"
                }}
            >
                {props.health_status}
            </Paragraph>
        </Space>
    )
};