import React, { useEffect, useState } from 'react';
import 'ol/ol.css';
import { MapBrowserEvent } from 'ol';
import IOverlayInfo from '../interfaces/IOverlayInfo';
import IMapInfo from '../interfaces/IMapInfo';
import Popup from './Popup';
import CardDTP from './CardDTP';
import { Modal } from 'antd';
const zeroOverlayInfo = {
    properties: {
        participants: [],
        transports: [],
        category: "",
        region: "",
        address: "",
        datetime: "",
        count_death: 0,
        count_injured: 0,
        count_participants: 0,
        severity: "",
        light: "",
        weather: [],
    },
    show: false,
};

export default function DTPMap(props: IMapInfo) {
    const [popupInfo, setPopupInfo] = useState<IOverlayInfo>(zeroOverlayInfo);
    const [cardInfo, setCardInfo] = useState<IOverlayInfo>(zeroOverlayInfo);
    const [error, setError] = useState("");

    const showPopup = (evt:MapBrowserEvent<UIEvent>) => {
        const feature = props.dtpMap.findFeature(evt.pixel);

        if (feature) {
            const featureInfo = props.dtpMap.getFeatures().find((elem) => {
                if (elem.id === feature.getId()) {
                    return elem;
                };
            });

            if (featureInfo !== undefined) {
                props.dtpMap.setPopupPosition(feature.get("geometry").flatCoordinates);
                document.body.style.cursor = "pointer";
                setPopupInfo({
                    ...featureInfo,
                    show: true,
                });
            } else {
                setError("Feature hasn't info");
            };
        } else {
            document.body.style.cursor = "default";
            setPopupInfo({
                ...popupInfo,
                show: false,
            });
        };
    };

    const showCard = (evt:MapBrowserEvent<UIEvent>) => {
        const feature = props.dtpMap.findFeature(evt.pixel);

        if (feature) {
            const featureInfo = props.dtpMap.getFeatures().find((elem) => {
                if (elem.id === feature.getId()) {
                    return elem;
                };
            });

            if (featureInfo !== undefined) {
                document.body.style.cursor = "default";
                props.dtpMap.lockPopup();
                setPopupInfo({
                    ...popupInfo,
                    show: false,
                });  
                setCardInfo({
                    ...featureInfo!,
                    show: true
                });
            } else {
                setError("Feature hasn't info");
            };
        };
    };
    
    useEffect(() => {
        props.dtpMap.setTarget("map");
        props.dtpMap.setPopupElement(document.getElementById("popup")!);
        props.dtpMap.setShowPopup(showPopup);
        props.dtpMap.setShowCard(showCard);
    }, []);

    useEffect(() => {
        if (!props.isFetching && !error) {
            props.dtpMap.unlockPopup();
            props.dtpMap.unlockCard();
            
            return () => {
                props.dtpMap.lockPopup();
                props.dtpMap.lockCard();
            };
        };
    }, [props.isFetching]);

    useEffect( () => {
        if(error){
            Modal.error({
                title: 'У нас возникла ошибка',
                content: `Подождите, cкоро починим`,
                okButtonProps: { style:{ display:"none" }},
                keyboard:false,
            });
        };
    }, [error]);

    return (
        <>
            <div id="map">
                <Popup {...popupInfo} />
                <Modal
                    visible={cardInfo.show}
                    closable={true} 
                    footer={null}
                    onCancel={() => {
                        setCardInfo({
                            ...cardInfo,
                            show: false,
                        });  
                        props.dtpMap.unlockPopup();
                    }}
                    width="95%"
                    bodyStyle={{
                        height:"800px"
                    }}
                    style={{
                        top: "65px",
                    }}
                >
                    <CardDTP {...cardInfo}/>
                </Modal>
            </div>
        </>
    );
};