import React, { useState, useEffect, useMemo } from 'react';
import IFilters from '../interfaces/IFilters'
import ITegsInfo from '../interfaces/ITegsInfo';
import Header from './Filters/Header';
import { Card, Space } from 'antd';
import moment from 'moment'
import Date from './Filters/Date';
import Role from './Filters/Role';
import Severity from './Filters/Severity';
import Tags from './Filters/Tags';

export default function Filters(props: IFilters) {
  const [dateFilter, setDateFilter] = useState([
    moment().add(-3, "year"),
    moment(),
  ]);
  const [roleFilter, setRoleFilter] = useState<string[]>([]);
  const [severityFilter, setSeverityFilter] = useState({
    easy: true,
    hard: true,
    death: true,
  });
  const [tegsFilter, setTegsFilter] = useState<ITegsInfo>({
    category: [],
    light: [],
    weather: [],
  })

  function setDate(date: moment.Moment, index: number) {
    const newDate:moment.Moment[] =[];
    if (index === 0){
      newDate[0] = moment(date);
      newDate[1] = dateFilter[1];
    } else {
      newDate[0] = dateFilter[0];
      newDate[1] = moment(date);
    };
    setDateFilter(newDate);
  };

  function addRoleFilter(role: string) {
    if (role !== "") {
      if (roleFilter.indexOf(role) === -1) {
        setRoleFilter([...roleFilter, role]);
      } else {
        setRoleFilter(
          roleFilter.filter((elem) => {
            if (elem !== role){
              return elem;
            };
          })
        );
      };
    } else {
      setRoleFilter([]);
    };
  };

  function addDateToUrl() {
    return `date_after=${dateFilter[0].format().substr(0, 10)}&date_before=${dateFilter[1].format().substr(0, 10)}`;
  };

  function addRoleToUrl() {
    let urlRole = ``;

    if (roleFilter.length !== 0) {
      urlRole += "&role=";
      roleFilter.forEach((elem, index) => {
        if (index !== roleFilter.length - 1) {
          urlRole += `${elem};`;
        };
      });
      urlRole += `${roleFilter[roleFilter.length - 1]}`;
    };

    return urlRole;
  };

  function addSeverityToUrl() {
    let urlSeverity =``;

    if (!(severityFilter.easy || severityFilter.hard || severityFilter.death)) {
      urlSeverity += "&severity=null";
    } else {
      urlSeverity += "&severity=";
      if (severityFilter.easy) {
        urlSeverity += "Легкий";
        if (severityFilter.hard || severityFilter.death) {
          urlSeverity += ";";
        };
      };
      if (severityFilter.hard) {
        urlSeverity += "Тяжёлый";
        if (severityFilter.death) {
          urlSeverity += ";";
        };
      };
      if (severityFilter.death) {
        urlSeverity += "С погибшими";
      };
    };

    return urlSeverity;
  };

  function addTegsToUrl(){
    let urlTegs =``;

    if(tegsFilter.category.length > 0){
      urlTegs += "&category=";
      tegsFilter.category.forEach((elem, index) => {
        if (index !== tegsFilter.category.length - 1) {
          urlTegs += `${elem};`;
        };
      });
      urlTegs += `${tegsFilter.category[tegsFilter.category.length - 1]}`;
    };
    if(tegsFilter.light.length > 0){
      urlTegs += "&light=";
      tegsFilter.light.forEach((elem, index) => {
        if (index !== tegsFilter.light.length - 1) {
          urlTegs += `${elem};`;
        };
      });
      urlTegs += `${tegsFilter.light[tegsFilter.light.length - 1]}`;
    };
    if(tegsFilter.weather.length > 0){
      urlTegs += "&weather=";
      tegsFilter.weather.forEach((elem, index) => {
        if (index !== tegsFilter.weather.length - 1) {
          urlTegs += `${elem};`;
        };
      });
      urlTegs += `${tegsFilter.weather[tegsFilter.weather.length - 1]}`;
    };

    return urlTegs;
  };

  const dateStr = useMemo(() => 
    addDateToUrl()
  ,[dateFilter]);

  const roleStr = useMemo(() => 
    addRoleToUrl()
  ,[roleFilter]);

  const severityStr = useMemo(() => 
    addSeverityToUrl()
  ,[severityFilter]);

  const tegsStr = useMemo(() => 
    addTegsToUrl()
  ,[tegsFilter]);

  useEffect(() => {
    let filters = "";
    
    filters +=  dateStr;
    filters +=  roleStr;
    filters +=  severityStr;
    filters +=  tegsStr;
    props.setFilters(filters);
  }, [dateStr, roleStr, severityStr, tegsStr]);

  return (
    <Card style={{
      position: "absolute",
      top: "10px",
      left: "10px",
      width: "428px",
      borderRadius: "20px",
      zIndex: 1,
    }}>
      <Space direction="vertical">
        <Header {...props}/>
        <Date {...{dateFilter, setDate, ...props}}/>
        <Role {...{roleFilter, addRoleFilter}}/>
        <Severity {...{severityFilter, setSeverityFilter}}/>    
        <Tags {...{setTegsFilter}}/>
      </Space>
    </Card>
  );
};