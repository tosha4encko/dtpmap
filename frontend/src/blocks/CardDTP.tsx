import React from 'react';
import IOverlayInfo from '../interfaces/IOverlayInfo';
import Heders from './Card/Heders';
import Tegs from './Card/Tags';
import Members from './Card/Members';
import { Space, Divider } from 'antd';

export default function CardDTP(props:IOverlayInfo) {

    return(
        <Space 
            id="cardDTP" 
            style={{
                display: props.show ? "block" : "none",
                width: "100%",
            }}
        >
            {props.show ?
                <Space direction="vertical"
                    style={{
                        position: "relative", 
                        width: "100%",
                        height: "100%",
                    }}
                >
                    <Heders {...props}/>
                    <Divider />
                    <Space 
                        direction="horizontal" 
                        align="start"
                        style={{
                            width: "100%",
                            height: "100%",
                        }}
                    >
                        <Tegs {...props.properties}/>
                        <Space 
                            style={{
                                width: "100%",
                                height: "100%",
                            }}
                        >
                            <Members {...props.properties}/>
                        </Space>
                    </Space>
                </Space>
            : <></>}
        </Space>
    );
};