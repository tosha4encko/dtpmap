import React, { useState, useEffect } from "react";
import ITegFilter from '../../interfaces/ITegFilter';
import ITegsInfo from '../../interfaces/ITegsInfo';
import { Typography, Space, Button, Checkbox, Modal, Badge } from 'antd';
const { Text } = Typography;

export default function TagsFilter(props: ITegFilter) {
    const [tegs, setTegs] = useState<ITegsInfo>({
        category: [],
        light: [],
        weather: [],
    });
    const [tegShow, setTegShow] = useState("");
    const [choosedCategory, setChoosedCategory] = useState<string[]>([]);
    const [choosedLight, setChoosedLight] = useState<string[]>([]);
    const [choosedWeather, setChoosedWeather] = useState<string[]>([]);
    const [ error, setError ] = useState("");

    function parseInfo(data: ITegsInfo) {
        const tegs: ITegsInfo = {
            category: data.category,
            light: data.light,
            weather:[],
        };
        const weather: string[] = [];

        data.weather.forEach((elem) => {
            for (const weather_ of elem){
                if (weather.indexOf(weather_) === -1){
                    weather.push(weather_);
                };
            };
        });
        tegs.weather = weather;

        return tegs;
    }

    function showTeg(tegName: string) {
        if (tegShow !== tegName){
            setTegShow(tegName);
        } else {
            setTegShow("");
        };
    };
    
    useEffect(() => {
        const url = `${process.env.REACT_APP_API_URL}api/v1/dtp/info`;

        fetch(url).then(async response => {
            const data = await response.json();
    
            if (response.ok) {
                setTegs(parseInfo(data));
            } else {
              setError("Response is not ok");
            };
        }).catch(() => {
            setError("Incorrect adres");
        })
    }, []);

    useEffect(() => {
        props.setTegsFilter({
            category: choosedCategory,
            light: choosedLight,
            weather: choosedWeather,
        });
    }, [choosedCategory, choosedLight, choosedWeather])

    useEffect(() => {
        if (error) {
            Modal.error({
            title: 'У нас возникла ошибка',
            content: error,
            okButtonProps: { style:{ display:"none" }},
            keyboard:false,
            });
        };
    }, [error]);

    return (
        <Space direction="vertical">
            <Text className="title">Фильтры</Text>
            <Space direction="horizontal">
                <Badge count={choosedCategory.length} style={{backgroundColor:"#1890ff", zIndex:10}}>
                    <Button
                        type={tegShow === "category" ? "primary" : "default"} 
                        onClick={() => showTeg("category")}
                    >                    
                        Типы ДТП
                    </Button>
                </Badge>
                <Badge count={choosedLight.length} style={{backgroundColor:"#1890ff", zIndex:10}}>
                    <Button
                        type={tegShow === "light" ? "primary" : "default"} 
                        onClick={() => showTeg("light")}
                    >
                        Дорожные условия
                    </Button>
                </Badge>    
                <Badge count={choosedWeather.length} style={{backgroundColor:"#1890ff", zIndex:10}}>
                    <Button
                        type={tegShow === "weather" ? "primary" : "default"}
                        onClick={() => showTeg("weather")}
                    >
                        Погода
                    </Button>
                </Badge>
            </Space>
            <Space 
                direction="vertical"
                style={{
                    display:tegShow === "category" ? "block" : "none",
                }}
                className="filter_teg"
            >
                {tegs.category.map((elem, index) => {
                    return(
                        <Checkbox 
                            key={index}
                            onChange={() => {
                                if(choosedCategory.indexOf(elem) === -1){
                                    setChoosedCategory([...choosedCategory, elem]);
                                } else {
                                    setChoosedCategory(
                                        choosedCategory.filter((item) => {
                                            if (item !== elem){
                                              return item;
                                            };
                                        })
                                    )
                                }
                            }}
                        >{elem}</Checkbox>
                    )
                })}
            </Space>
            <Space 
                direction="vertical"
                style={{
                    display: tegShow === "light" ? "block" : "none",
                }}
                className="filter_teg"
            >
                {tegs.light.map((elem, index) => {
                    return(
                        <Checkbox 
                            key={index + tegs.category.length}
                            onChange={() => {
                                if(choosedLight.indexOf(elem) === -1){
                                    setChoosedLight([...choosedLight, elem]);
                                } else {
                                    setChoosedLight(
                                        choosedLight.filter((item) => {
                                            if (item !== elem){
                                              return item;
                                            };
                                        })
                                    )
                                }
                            }}
                        >{elem}</Checkbox>
                    )
                })}
            </Space>
            <Space 
                direction="vertical"
                style={{
                    display: tegShow === "weather" ? "block" : "none",
                }}
                className="filter_teg"
            >
                {tegs.weather.map((elem, index) => {
                    return(
                        <Checkbox 
                            key={index + tegs.category.length + tegs.light.length}
                            onChange={() => {
                                if(choosedWeather.indexOf(elem) === -1){
                                    setChoosedWeather([...choosedWeather, elem]);
                                } else {
                                    setChoosedWeather(
                                        choosedWeather.filter((item) => {
                                            if (item !== elem){
                                              return item;
                                            };
                                        })
                                    )
                                }
                            }}
                        >{elem}</Checkbox>
                    )
                })}
            </Space>
        </Space>
    );
};