import React from "react";
import IHeaderFilter from '../../interfaces/IHeaderFilter';
import { Typography, Space, Spin } from 'antd';
const { Text } = Typography;

export default function Header(props: IHeaderFilter) {
  let numberOfDeath = 0;
  let numberOfInjured = 0;

  props.features.forEach((elem) => {
    numberOfDeath+=elem.properties.count_death;
    numberOfInjured+=elem.properties.count_injured;
  });

  return(
    <Space direction="horizontal" >
      <Space direction="vertical" style={{marginRight:"20px", alignItems:"center"}}>
        <Text className="title">Дтп</Text>
        <Text className="title" strong>{props.features.length}</Text>
      </Space>
      <Space direction="vertical" style={{marginRight:"20px", alignItems:"center"}}>
        <Text className="title">Пострадали</Text>
        <Text style={{color:"#e92828"}} className="title" strong>{numberOfInjured}</Text>
      </Space>
      <Space direction="vertical" style={{marginRight:"20px", alignItems:"center"}}>
        <Text className="title">Погибли</Text>
        <Text style={{color:"#6c0000"}} className="title" strong>{numberOfDeath}</Text>
      </Space>
      <Spin
        size="large" 
        spinning={ props.isFetching }
        style={{
          marginTop: "12px",
        }}
      >
      </Spin>
    </Space>
  );
};