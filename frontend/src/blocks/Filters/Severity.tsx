import React from "react";
import ISeverityFilter from '../../interfaces/ISeverityFilter';
import { Checkbox, Typography, Space, } from 'antd';
const { Text } = Typography;

export default function SeverityFilter(props: ISeverityFilter) {

    return(
        <Space direction="vertical">
            <Text className="title">Степень ДТП</Text>
            <Space direction="horizontal">
                <Checkbox 
                    checked={props.severityFilter.easy}
                    onChange={() => {
                        props.setSeverityFilter({
                            ...props.severityFilter,
                            easy: !props.severityFilter.easy,
                        })
                    }}
                >Легкий</Checkbox>
                <Checkbox 
                    checked={props.severityFilter.hard} 
                    onChange={() => {
                        props.setSeverityFilter({
                            ...props.severityFilter,
                            hard: !props.severityFilter.hard,
                        })
                    }}
                >Тяжёлый</Checkbox>
                <Checkbox 
                    checked={props.severityFilter.death} 
                    onChange={() => {
                        props.setSeverityFilter({
                            ...props.severityFilter,
                            death: !props.severityFilter.death,
                        })
                    }}
                >С погибшими</Checkbox>
            </Space>
        </Space>
    );
};