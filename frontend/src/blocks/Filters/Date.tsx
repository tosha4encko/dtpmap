import React, { useEffect, useState } from "react";
import IDateFilter from '../../interfaces/IDateFilter'

import { Typography, Space, DatePicker } from 'antd';
import moment from "moment";
const { Text } = Typography;

export default function DateFilter(props: IDateFilter) {
    const [isOpenDPickers, setIsOpenDPickers] = useState({
        after: false,
        before: false,
    })

    useEffect(() => {
        if(!isOpenDPickers.after && !isOpenDPickers.before){
            clearTimeout(props.fetchTimeout);
            props.setFetchTimeout(
                setTimeout(() => {
                    props.setIsAllowFetch(true);
                }, 3000)
              )
        } else {
            clearTimeout(props.fetchTimeout);
        }
    }, [isOpenDPickers])
    
    return(
        <Space direction="vertical">
            <Text className="title">Период проишествий</Text>
            <Space direction="horizontal">
            <DatePicker
                allowClear={false}
                defaultValue={props.dateFilter[0]}
                disabledDate={(current) => {
                    return current > props.dateFilter[1]
                }}
                onOpenChange={(isOpen) => {
                    setIsOpenDPickers({
                        ...isOpenDPickers,
                        after:isOpen,
                    })
                }}
                onChange={(date) => {
                    props.setDate(date!,0);
                }}
            />
            <DatePicker
                allowClear={false}
                defaultValue={props.dateFilter[1]}
                disabledDate={(current) => {
                    return current < props.dateFilter[0] || current > moment()
                }}
                onOpenChange={(isOpen) => {
                    setIsOpenDPickers({
                        ...isOpenDPickers,
                        before:isOpen,
                    })
                }}
                onChange={(date) => {
                    props.setDate(date!,1);
                }}
            />
            </Space>
        </Space>
    );
};