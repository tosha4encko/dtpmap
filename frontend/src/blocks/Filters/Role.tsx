import React from "react";
import IRoleFilter from '../../interfaces/IRoleFilter';
import { Typography, Space, Button } from 'antd';
const { Text } = Typography;

export default function RoleFilter(props: IRoleFilter) {

    return(
        <Space direction="vertical">
            <Text className="title">Участники ДТП</Text>
            <Space wrap style={{width: "380px"}}>
                <Button
                    type={props.roleFilter.length === 0 ? "primary" : "default"}
                    onClick={() => props.addRoleFilter("")}
                >Все участники</Button>
                <Button
                    type={props.roleFilter.indexOf("Пешеход") !== -1 ? "primary" : "default"}
                    onClick={() => props.addRoleFilter("Пешеход")}
                >Пешеходы</Button>
                <Button
                    type={props.roleFilter.indexOf("Велосипедист") !== -1 ? "primary" : "default"}
                    onClick={() => props.addRoleFilter("Велосипедист")}
                >Велосипедисты</Button>
          </Space>             
        </Space>
    );
};