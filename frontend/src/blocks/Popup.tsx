import React from 'react';
import IOverlayInfo from '../interfaces/IOverlayInfo';
import moment from 'moment';
import { Card, Typography, Space } from 'antd';
const { Text } = Typography;

export default function Popup(props: IOverlayInfo) {

    return (
        <Card 
            id="popup"
            style={{
                display: props.show ? "block" : "none",
                borderRadius: "20px",
            }}
        >
            {props.show ?
                <Space direction="vertical">
                    <Text strong>{props.properties.category}</Text>
                    <Text>{moment(props.properties.datetime).format("D.MM.YYYY hh:mm")}</Text>
                    <Text>{props.properties.address}</Text>
                    {props.properties.count_death > 0 ?
                        <Text
                            style={{
                                color: "#6c0000"
                            }}
                        >{props.properties.count_death} человек погиб{props.properties.count_death === 1 ? "" : "ло"}</Text>
                    : <></>}
                    {props.properties.count_injured > 0 ?
                        <Text
                            style={{
                                color: props.properties.severity === "Легкий" ? "#008080" : "#e92828"
                            }}
                        >{props.properties.count_injured} человек пострадал{props.properties.count_injured === 1 ? "" : "о"}</Text>
                    : <></>}
                </Space>
            : <></>}
        </Card>
    );
};