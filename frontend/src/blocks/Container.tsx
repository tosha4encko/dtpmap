import React, { useState, useEffect, useMemo } from "react";
import IFeature from '../interfaces/IFeature';
import Filters from "./Filters";
import DTPMap from "./DTPMap";
import DTPMapObj from '../classes/DTPMap'; 
import { useGeographic } from 'ol/proj';
import { Modal } from 'antd';

export default function Container(){
  const [features, setRes] = useState<IFeature[]>([]);
  const dtpMap = useMemo(() => new DTPMapObj(), []);
  const [ isFetching, setIsFetching] = useState(true);
  const [ error, setError ] = useState("");
  const [filters, setFilters] = useState("");
  const [isAllowFetch, setIsAllowFetch] = useState(true);
  const [fetchTimeout, setFetchTimeout] = useState<NodeJS.Timeout>(setTimeout(()=>{}, 2000));

  const changeIsFetching = (bool: boolean) => {
    setIsFetching(bool);
  };

  const changeСurrentFeatures = (features: IFeature[]) => {
    setRes(features);
  };

  const throwError = (error: string) => {
    setError(error);
  }

  useGeographic();

  useEffect(() => {
    dtpMap.setChangeIsFetching(changeIsFetching);
    dtpMap.setChangeСurrentFeatures(changeСurrentFeatures);
    dtpMap.setThrowError(throwError);
  }, [])

  useEffect(() => {
    clearTimeout(fetchTimeout);
    setIsAllowFetch(false);
    setFetchTimeout(
      setTimeout(() => {
        setIsAllowFetch(true);
      }, 2000)
    )  
  }, [filters])

  useEffect(() => {
    if (isAllowFetch) {
      dtpMap.setFilters(filters);
    }
  }, [isAllowFetch]);

  useEffect(() => {
    if (error) {
      Modal.error({
        title: 'У нас возникла ошибка',
        content: "Подождите, скоро починим",
        okButtonProps: { style:{ display:"none" }},
        keyboard:false,
      });
    };
  }, [error]);
  
  return(
    <>
      {!error ?
        <>
          <Filters {...{features, fetchTimeout, isFetching, setFilters, setIsAllowFetch, setFetchTimeout}}/>
          <DTPMap {...{dtpMap, isFetching}} />
        </>
      : <></>}  
    </>
  );
};