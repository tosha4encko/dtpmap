import DTPMap from '../classes/DTPMap';

interface MapInfo {
  dtpMap: DTPMap,
  isFetching: boolean,
};

export default MapInfo;