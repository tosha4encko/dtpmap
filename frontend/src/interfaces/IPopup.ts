import Properties from './IProperties';

interface Popup {
    properties: Properties,
    show: boolean,
};

export default Popup;