import Feature from './IFeature';

interface HeaderFilter {
  features: Feature[],
  isFetching: boolean,
};

export default HeaderFilter;
