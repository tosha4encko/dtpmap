interface Login {
    setAllowEnter: React.Dispatch<React.SetStateAction<boolean>>,
    setAccessToken: React.Dispatch<React.SetStateAction<string | undefined>>,
    setRefreshToken: React.Dispatch<React.SetStateAction<string | undefined>>,
};

export default Login;