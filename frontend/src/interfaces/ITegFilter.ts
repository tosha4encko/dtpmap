import TegsInfo from './ITegsInfo';

interface TegFilter{
    setTegsFilter: React.Dispatch<React.SetStateAction<TegsInfo>>,
};

export default TegFilter;