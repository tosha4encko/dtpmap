import Participant from './IParticipant';
import Transport from './ITransport';

interface Properties {
    participants: Participant[],
    transports: Transport[],
    category: string,
    region: string,
    address: string,
    datetime: string,
    count_death: number,
    count_injured: number,
    count_participants: number,
    severity: string,
    light: string,
    weather: string[],
};

export default Properties;