import Properties from './IProperties';
import Geometry from './IGeometry';

interface Feature {
    id: number,
    geometry: Geometry,
    properties: Properties,
};

export default Feature;