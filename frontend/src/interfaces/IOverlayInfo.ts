import Properties from './IProperties';

interface OverlayInfo {
    properties: Properties,
    show: boolean,
};

export default OverlayInfo;