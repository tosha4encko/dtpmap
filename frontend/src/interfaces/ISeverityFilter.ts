interface SeverityFilter{
    severityFilter:{
        easy: boolean,
        hard: boolean,
        death: boolean,
    },
    setSeverityFilter:React.Dispatch<React.SetStateAction<{
        easy: boolean,
        hard: boolean,
        death: boolean,
    }>>,
};

export default SeverityFilter;