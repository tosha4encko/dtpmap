import Participant from './IParticipant';

interface Participants{
    participants: Participant[],
};

export default Participants;