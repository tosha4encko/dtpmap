import Feature from './IFeature';

interface Filters {
    features: Feature[],
    fetchTimeout: NodeJS.Timeout,
    isFetching: boolean,
    setFilters: React.Dispatch<React.SetStateAction<string>>,
    setFetchTimeout: React.Dispatch<React.SetStateAction<NodeJS.Timeout>>
    setIsAllowFetch: React.Dispatch<React.SetStateAction<boolean>>,
};

export default Filters;