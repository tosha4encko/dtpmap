interface RoleFilter {
    roleFilter: string[],
    addRoleFilter: (role: string) => void,
};

export default RoleFilter;