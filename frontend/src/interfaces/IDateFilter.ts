interface DateFilter {
    dateFilter: moment.Moment[],
    fetchTimeout: NodeJS.Timeout,
    setDate: (date: moment.Moment, index: number) => void,
    setFetchTimeout: React.Dispatch<React.SetStateAction<NodeJS.Timeout>>
    setIsAllowFetch: React.Dispatch<React.SetStateAction<boolean>>,
};

export default DateFilter;