interface Participant {
    id: number,
    role: string,
    health_status: string,
    gender: string,
    violations: string[],
    transport: number | null,
    dtp: number | null,
};

export default Participant;