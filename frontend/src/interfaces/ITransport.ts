import Participant from './IParticipant';

interface Transport {
    id: number,
    participants: Participant[],
    year: number,
    brand: string,
    color: string,
    dtp: number,
};

export default Transport;