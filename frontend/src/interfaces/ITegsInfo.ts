interface TegsInfo{
    category: string[],
    weather: string[],
    light: string[],
};

export default TegsInfo;