import React, { useEffect, useState } from 'react';
import Login from './blocks/Login';
import Container from './blocks/Container';
import Cookies from 'js-cookie';
import './blocks/style/style.css';
import 'antd/dist/antd.css';
import { Modal, Spin } from 'antd';

export default function App() {
  const [accessToken, setAccessToken] = useState(Cookies.get("accessToken"));
  const [refreshToken, setRefreshToken] = useState(Cookies.get("refreshToken"));
  const [allowEnter, setAllowEnter] = useState(false);
  const [isFetching, setIsFetching] = useState(true);
  const [error, setError] = useState("");

  function refresh() {
    const params = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        refresh: refreshToken,
      }),
    };

    fetch(`${process.env.REACT_APP_API_URL}api/token/refresh/`, params).then(async response => {
      const data = await response.json();
      if (response.ok) {
        Cookies.set("accessToken", data.access);
        setAccessToken(data.access);
      } else {
        Cookies.set("accessToken", "");
        Cookies.set("refreshToken", "");
        setAccessToken("");
        setRefreshToken("");
        setIsFetching(false);
      };
    }).catch( (error) => {
      setError("Error connect");
    });
  };

  function access() {
    const params = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        token: accessToken,
      }),
    };

    fetch(`${process.env.REACT_APP_API_URL}api/token/verify/`, params).then(response => {
      if (response.ok) {
        setAllowEnter(true);
      } else {
        refresh();
      };
    }).catch( (error) => {
      setError("Error connect");
    });
  };

  useEffect(() => {
    if (accessToken && refreshToken) {
      access();
    } else {
      setIsFetching(false);
    }
  }, []);

  useEffect(() => {
    if (error) {
      Modal.error({
        title: 'У нас возникла ошибка',
        content: `Подождите, cкоро починим`,
        okButtonProps: { style:{ display:"none" }},
        keyboard:false,
      });
    };
  }, [error]);
 
  return (
    <div className="App">
      {allowEnter ? 
        <Container />
      :
        isFetching ? 
          <Spin
            tip="Loading..." 
            size="large" 
            spinning={isFetching && !error}
            style={{
              height: "100vh",
              maxHeight: "100vh",
              width: "100vw",
            }}
          >
            <></>
          </Spin>
        :
          <Login {...{setAllowEnter, setAccessToken, setRefreshToken}}/>
      }
    </div>
  );
};
