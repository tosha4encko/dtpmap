Проект представляет собой веб-приложение для визуализации ДТП на карте Ростовской области  
Структура и данные взяты из http://stat.gibdd.ru/  
Серверная часть реализована на Python/Django/DjangoRestFramework, клиентская - TypeScript/React/OpenLayers, в качестве бд используется PostgreSQL/PostGIS, 
