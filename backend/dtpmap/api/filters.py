from django_filters import rest_framework as filters
from django.db.models import Q
from .models import Dtp
from django.contrib.gis.geos import Polygon


class DtpFilter(filters.FilterSet):
    date = filters.DateTimeFromToRangeFilter('datetime')
    severity = filters.CharFilter(method='severity_filter')
    role = filters.CharFilter(method='role_filter')
    category = filters.CharFilter(method='category_filter')
    weather = filters.CharFilter(method='weather_filter')
    light = filters.CharFilter(method='light_filter')
    bbox = filters.CharFilter(method='bbox_filter')

    def role_filter(self, queryset, name, value):
        values = value.split(';')
        return queryset.filter(
            Q(participant__role__in=values) | Q(transport__participant__role__in=values)
        )

    def severity_filter(self, queryset, name, value):
        values = value.split(';')
        return queryset.filter(severity__in=values)

    def category_filter(self, queryset, name, value):
        values = value.split(';')
        return queryset.filter(category__in=values)

    def weather_filter(self, queryset, name, value):
        values = value.split(';')
        return queryset.filter(weather__contained_by=values)

    def light_filter(self, queryset, name, value):
        values = value.split(';')
        return queryset.filter(light__in=values)

    def bbox_filter(self, queryset, name, value):
        p1x, p1y, p2x, p2y = (float(n) for n in value.split(','))
        bbox = Polygon.from_bbox((p1x, p1y, p2x, p2y))
        return queryset.filter(point__contained=bbox)

    class Meta:
        model = Dtp
        fields = ['date', 'severity', 'role', 'category', 'weather', 'light', 'bbox']
