from django.db import models
from django.contrib.gis.db.models import PointField
from django.db.models.deletion import CASCADE, PROTECT
from django.contrib.postgres.fields import ArrayField


class Dtp(models.Model):
    category = models.CharField(max_length=128)
    region = models.CharField(max_length=128)
    address = models.CharField(max_length=128)
    datetime = models.DateTimeField()

    count_death = models.IntegerField(default=0)
    count_injured = models.IntegerField(default=0)
    count_participants = models.IntegerField(default=0)
    severity = models.CharField(max_length=20, default='Без пострадавших')
    light = models.CharField(max_length=64)
    weather = ArrayField(models.CharField(max_length=64), size=10)

    point = PointField()

    def __str__(self):
        return f"{self.pk}"

    class Meta:
        verbose_name = "ДТП"
        verbose_name_plural = "ДТП"


class Transport(models.Model):
    year = models.IntegerField(default=0, null=True)
    brand = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    dtp = models.ForeignKey(Dtp, on_delete=CASCADE, null=True)
    
    def __str__(self):
        return f"{self.pk}"

    class Meta:
        verbose_name = "Транспортное средство"
        verbose_name_plural = "Транспортные средства"


class Participant(models.Model):
    role = models.CharField(max_length=128)
    health_status = models.CharField(max_length=300)
    gender = models.CharField(max_length=300)
    violations = ArrayField(models.CharField(max_length=500), size=10)
    transport = models.ForeignKey(Transport, on_delete=CASCADE, null=True)
    dtp = models.ForeignKey(Dtp, on_delete=CASCADE, null=True)
    
    def __str__(self):
        return f"{self.pk}"

    class Meta:
        verbose_name = "Участник ДТП"
        verbose_name_plural = "Участники ДТП"