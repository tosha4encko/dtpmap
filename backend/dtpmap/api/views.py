from rest_framework import generics
from .serializers import DtpSerializer
from .filters import DtpFilter
from django.http.response import JsonResponse, StreamingHttpResponse
from .models import Dtp
from rest_framework_gis.pagination import GeoJsonPagination
from collections import OrderedDict
import json
from django_filters.rest_framework import DjangoFilterBackend


class DtpListView(generics.ListAPIView):
    queryset = Dtp.objects.all()
    serializer_class = DtpSerializer
    filter_backends =[DjangoFilterBackend]
    filter_field =['date','severity','role' ,'category','weather ','light','bbox']
    filterset_class = DtpFilter

    def get(self, request):
        stream = self.build_response()
        return StreamingHttpResponse(stream)

    def build_response(self) -> str:
        data = self._get_paginated_data()
        yield '{"type": "FeatureCollection",'
        yield f'"count": {data["count"]}, "next":"{data["next"]}",'
        yield '"features":['

        delimiter_position = 0
        for feature in data['results']['features']:
            if delimiter_position != 0:
                yield ','
            yield json.dumps(feature, ensure_ascii=False).encode('utf8').decode()
            delimiter_position += 1
        yield """]}"""

    def _get_paginated_data(self):
        paginator = DtpPaginator()
        paginator.page_size = 1000
        result_page = paginator.paginate_queryset(self.filter_queryset(self.queryset), self.request)
        serializer = self.serializer_class(result_page, many=True)
        data = paginator.get_paginated_data(serializer.data)
        return data

class DtpPaginator(GeoJsonPagination):
    def get_paginated_data(self, data):
        return OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('results', data)
        ])


def get_dtp_info(request):
    info_dict = dict()
    fields = ['category', 'weather', 'light', 'address']
    for field in fields:
        info_dict[field] = list(Dtp.objects.distinct(field).values_list(field, flat=True))

    return JsonResponse(info_dict, safe=False, json_dumps_params={'ensure_ascii': False})