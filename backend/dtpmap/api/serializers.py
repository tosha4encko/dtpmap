from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework import serializers
from .models import Dtp, Participant, Transport


class ParticipantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Participant
        fields = '__all__'


class TransportSerializer(serializers.ModelSerializer):
    participants = ParticipantSerializer(source='participant_set', many=True)

    class Meta:
        model = Transport
        fields = '__all__'


class DtpSerializer(GeoFeatureModelSerializer):
    participants = ParticipantSerializer(source='participant_set', many=True)
    transports = TransportSerializer(source='transport_set', many=True)

    class Meta:
        model = Dtp
        fields = '__all__'
        geo_field = "point"
