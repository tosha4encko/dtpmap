from .models import Dtp, Participant, Transport
from django.contrib.gis.geos import Point
from datetime import datetime
from django.db import transaction
from typing import List
import json
import os


def clear():
    Participant.objects.all().delete()
    Transport.objects.all().delete()
    Dtp.objects.all().delete()


def create_dtp(properties: dict, coordinates: List[float]) -> Dtp:
    dtp = Dtp.objects.create(
        region=properties['region'],
        address=properties['address'],
        severity=properties['severity'],
        category=properties['category'],
        count_participants=properties['participants_count'],
        count_death=properties['dead_count'],
        light=properties['light'],
        weather=properties['weather'],
        count_injured=properties['injured_count'],
        datetime=datetime.strptime(properties['datetime'], '%Y-%m-%d %H:%M:%S'),
        point=Point(coordinates),
    )
    return dtp


def create_drivers(drivers: List[dict]) -> List[Participant]:
    list_drivers = []
    for driver_data in drivers:
        driver = Participant.objects.create(
            role=driver_data['role'],
            gender=driver_data['gender'],
            health_status=driver_data['health_status'],
            violations=driver_data['violations'],
        )
        list_drivers.append(driver)

    return list_drivers


def create_transports(vehicles: List[dict]) -> List[Participant]:
    transports = []
    for transport in vehicles:
        tmp_transport = Transport.objects.create(
            year=transport['year'],
            brand=transport['brand'],
            color=transport['color'],
        )
        for driver in create_drivers(transport['participants']):
            tmp_transport.participant_set.add(driver)

        transports.append(tmp_transport)

    return transports


def create_pedestrians(pedestrians_data: List[dict]) -> List[Participant]:
    pedestrians = []
    for person in pedestrians_data:
        pedestrian = Participant.objects.create(
            role=person['role'],
            gender=person['gender'],
            health_status=person['health_status'],
            violations=person['violations'],
        )
        pedestrians.append(pedestrian)
    return pedestrians


@transaction.atomic
def run(count_of_data: int):
    with open(os.getcwd() + "/api/data/rostovskaia-oblast.geojson", "r") as file:
        data = json.load(file)
        count = 0
        for feature in data['features']:
            count += 1

            try:
                with transaction.atomic():
                    dtp = create_dtp(feature['properties'], feature['geometry']['coordinates'])
                    transports = create_transports(feature['properties']['vehicles'])
                    pedestrians = create_pedestrians(feature['properties']['participants'])

                    for transport in transports:
                        transport.dtp = dtp
                        transport.save()

                    for pedestrian in pedestrians:
                        pedestrian.dtp = dtp
                        pedestrian.save()

            except:
                print("Incorrect data")

            if count == count_of_data:
                break
