from django.contrib import admin
from .models import *


class ParticipantAdmin(admin.ModelAdmin):
    list_display = ('pk', 'role', 'violations', 'health_status', 'gender')
    search_fields = ('pk', 'role', 'violations', 'health_status', 'gender')


class DtpAdmin(admin.ModelAdmin):
    list_display = (
        'pk', 'region', 'address', 'datetime', 'severity', 'weather', 'light', "count_death", "count_injured",
        'count_participants')
    search_fields = (
        'pk', 'region', 'datetime', 'severity', 'weather', 'light', "count_death", "count_injured",
        'count_participants')


class TransportAdmin(admin.ModelAdmin):
    list_display = ('pk', 'year', 'brand', 'color')
    search_fields = ('pk', 'year', 'brand', 'color')


admin.site.register(Dtp, DtpAdmin)
admin.site.register(Participant, ParticipantAdmin)
admin.site.register(Transport, TransportAdmin)
